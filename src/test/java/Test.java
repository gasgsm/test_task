import io.restassured.path.json.JsonPath;
import com.jcabi.xml.XML;
import com.jcabi.xml.XMLDocument;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import static io.restassured.RestAssured.get;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.Assert.assertEquals;



public class Test {

    public static String XML_Program_URL = "http://www.vsetv.com/export/megogo/epg/3.xml";
    public static String JSON_Program_URL = "http://epg.megogo.net/channel?external_id=295";


    @org.junit.Test
    public void compareXML_JSON_programs() {
        //используя rest assured сохраняем респонсы в string
        String xml = get(XML_Program_URL).andReturn().body().asString();
        String json = get(JSON_Program_URL).asString();
        //через rest assured не получилось с xpath, поэтому тут используется com.jcabi.xml
        XML x = new XMLDocument(xml);
        //вытягиваем все объекты внутри programs array
        List<ArrayList> jsonPrg = JsonPath.from(json).getList("data.programs");
        List<HashMap> t = jsonPrg.get(0);

        for (int i = 0; i < t.size(); i++) {
            //готовим start_date, end_date для использования в xpath
            Integer tmp = (Integer) t.get(i).get("start_timestamp");
            Long tmp2 = tmp * 1000L;
            Date startDate = new Date(tmp2);
            String xmlStartDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startDate)) + " +0200";
            Integer tmp3 = (Integer) t.get(i).get("end_timestamp");
            Long tmp4 = tmp3 * 1000L;
            Date endDate = new Date(tmp4);
            String xmlEndDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endDate)) + " +0200";
            //по тайтлу будем валидировать
            String title = (String) t.get(i).get("title");
            String xmlTitle = x.xpath("//programme[contains(@start,'" + xmlStartDate + "') and contains(@stop,'" + xmlEndDate + "')]/title/text()").get(0);
            System.out.println("Json title: " + title + "    XML title: " + xmlTitle);
            assertEquals(title, xmlTitle);
        }

    }

    @org.junit.Test
    public void jsonValidation(){
        //json schema создал используя https://www.npmjs.com/package/generate-schema
        //валидация через rest assured
        get(JSON_Program_URL).then().assertThat().body(matchesJsonSchemaInClasspath("schema.json"));
    }
}